import os
from dotenv import load_dotenv, dotenv_values

load_dotenv()

print('[INFO] Load with getenv')
print(os.getenv("MY_SECRET_KEY"))
print(os.getenv("COMBINED"))
print(os.getenv("MAIL"))

config = dotenv_values(".env")
print('[INFO] Load with object')
print(config['MAIL'])
